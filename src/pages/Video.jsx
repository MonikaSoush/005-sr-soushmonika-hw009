import{Container,Button} from "react-bootstrap";
import Menu from "../components/Menu";
import{Link, Switch,useLocation,useRouteMatch}from "react-router-dom";



export default function Video(){
    const {url,path} = useRouteMatch();
    return (
      <div>
        <Menu />
        <Container className="mt-3">
          <h3>Video</h3>
          <ButtonGroup>
            <Link>
              <Button variant="secondary" as={Link} to={"${url}/movie"}>
                Movie
              </Button>
            </Link>
            <Link>
              <Button variant="secondary" as={Link} to={"${url}/animation"}>
                Animation
              </Button>
            </Link>
          </ButtonGroup>
          <Switch>
            <Route path={"${path}/movie"}>
              <Movie />
            </Route>
            <Route path={"${path}/animation"}>
              <Movie />
            </Route>
          </Switch>
        </Container>
      </div>
    );
}
function VideoCate(props){
    const {types,title}=props;
    const{url}=useRouteMatch();
    const{search}=useLocation();
    const query=new URLSearchParams(search);
    return(
        <div>
            <h3>{title}</h3>
            <ButtonGroup>
                {type.map((item,index)=>{
                    return(
                        <Button
                        variant="secondary"
                        key={index}
                        as={Link}
                        to={'${url}?type=${item}'}>
                            {item}
                        </Button>
                    );
                })}
            </ButtonGroup>
            <h5>Please Choose Category: {query.get("type")}</h5>
        </div>
    );
}
function Movie(){
    const types=["Adventure","Crime","Action","Romance"]
    return<VideoCate title="Movie Category" types={types}>;

   
}
function Animation(){
    const types=["Adventure","Crime","Action","Romance"]
    return<VideoCate title="Movie Category" types={types}>;

    
}
