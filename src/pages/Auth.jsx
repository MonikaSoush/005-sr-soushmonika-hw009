import React from 'react'
import Menu from "../components/Menu";
import FormLogin from "../components/FormLogin";
import {myAuth} from "../components/AuthProvider"

export default function Auth() {
  const {theAuth}=myAuth();
    return (
      <div>
        <Menu />
        <FormLogin/>
      </div>
    );
}
