import React from 'react'
import Menu from "../components/Menu";
import { Button, Container } from "react-bootstrap";
import { Redirect } from "react-router";
import { myAuth } from "../components/AuthProvider";


export default function Welcome() {
  const { TheAuth } = myAuth();
    return (
      <div>
        <h1>Log In</h1>
        <Container>
          {TheAuth ? (
            <Form>
              <Menu />
              <h1>Welcome</h1>
              <Button variant="primary" type="submit">
                Log out
              </Button>
            </Form>
          ) : (
            <Redirect to="/auth" />
          )}
        </Container>
      </div>
    );
}
