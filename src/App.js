import React, { useState } from "react";
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from './pages/Home';
import  Video from './pages/Video';
import  Account from './pages/Account';
import  Auth from './pages/Auth';
import  Welcome from './pages/Welcome';
import Detail from './pages/Detail';
import {
  BrowserRouter,
  Switch,
  Route,
} from "react-router-dom";
import useToken from "./component/useToken";

function setToken(userToken) {
sessionStorage.setItem("token", JSON.stringify(userToken));
}

function getToken() {
const tokenString = sessionStorage.getItem("token");
const userToken = JSON.parse(tokenString);
return userToken?.token;
}

export default function App(){
const { token, setToken } = useToken();
const token = getToken();

  const [token, setToken] = useState();
  if (!token) {
    return <Login setToken={setToken} />;
  }
return (
  <Auth>
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/video" component={Video} />
        <Route path="/account" component={Account} />
        <Route path="/welcome" component={Welcome} />
        <Route path="/auth" component={Auth} />
        <Route path="/detail/:id" component={Detail} />
      </Switch>
    </BrowserRouter>
  </Auth>
);
}

export default App;