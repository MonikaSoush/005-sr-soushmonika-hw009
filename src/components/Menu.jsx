import React from "react";
import {Navbar,Nav, NavDropdown,Form,FormControl,Button} from 'react-bootstrap';
import { Link } from "react-router-dom";

export default function Menu() {


  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <NavDropdown className="me-auto">
          <h2>React-Router</h2>
          <Nav.Link as={Link} to={"/Home/${id}"}>
            Home
          </Nav.Link>
          <Nav.Link as={Link} to="/Video">
            Video
          </Nav.Link>
          <Nav.Link as={Link} to="/Account">
            Account
          </Nav.Link>
          <Nav.Link as={Link} to="/Welcome">
            Welcome
          </Nav.Link>
          <Nav.Link as={Link} to="/Auth">
            Auth
          </Nav.Link>

        <Form inline>
          <FormControl type="text" placeholder="Search" className="mr-sm-2" />
          <Button variant="outline-success">Search</Button>
        </Form>
      </Navbar.Collapse>
    </Navbar>
  );
}
