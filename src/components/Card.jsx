import { Link } from "react-router-dom";

import { Button, Card } from "react-bootstrap";

export default function (props) {
  const { story } = props;
  return (
    <Card>
      <Card.Img variant="top" src={story.img} />
      <Card.Body>
        <Card.Title>{story.title}</Card.Title>
        <Card.Text>{story.type}</Card.Text>
        <Link to={`/detail/${story.id}`}>
          <Button variant="secondary">Read</Button>
        </Link>
      </Card.Body>
    </Card>
  );
}
