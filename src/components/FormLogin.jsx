import React from 'react'
import { Container,Form,Button } from 'react-bootstrap'
import PropTypes from "prop-types";


async function loginUser(credentials) {
  return fetch("http://localhost:8080/FormLogin", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(credentials),
  }).then((data) => data.json());
}


export default function FormLogin({ setToken }) {

    const [email, setEmail] = useState("mocha@gmai.com");
    const [pw, setPw] = useState(123);

const handleSubmit = async (e) => {
  e.preventDefault();
  const token = await loginUser({
    email,
    pw,
  });
  setToken(token);
};


  return (
    <div>
      <h1>Log In</h1>
      <Container>
        <Form onSubmit={handleSubmit}>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              onChange={(e) => setEmail(e.target.value)}
            />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              onChange={(e) => setPw(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicCheckbox">
            <Form.Check type="checkbox" label="Check me out" />
          </Form.Group>
          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>
      </Container>
    </div>
  );
}
FormLogin.propTypes = {
  setToken: PropTypes.func.isRequired,
};