import React from "react";
import ReactROM from "react-dom";
import App from "./App";

ReactROM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
